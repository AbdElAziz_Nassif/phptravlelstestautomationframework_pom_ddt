# (UI & API Testing)

### Tools Used For UI testing:
```bash
Java + Maven + TestNG + Selenium (UI automation) + ExtentReport (custom report generation) 
```

### How To Run From Command Line
```bash
For Running in single browser (i.e chrome): 
              mvn test -Pregression
For Running in parallel mode(i.e chrome & firefox):
              mvn test -PparRegression
```

### Tools Used For API testing:
```bash
Java + Maven + TestNG + HttpClient Java class for (Api Testing).
```

### Design Patterns Used in this framework:
```bash
Page Object Model (POM) Design and Data Driven Test (DDT) design using DataProvider TestNG annotation
```

### Scenarios For UI Testing 
Scenarios: Registration
```bash
1 - User try to register with valid data then login to validate his registration.
    This scenaio will run two consecutive times.
    - user register validly.
    - user logout.
    - user login again.
    - user log out again (for the sake of DDT design).
2 - User try to register with invalid data to check the error messaage.
    - user register invalidly and check error messages (Error Email Formatting).
    - user register invalidly and check error messages (Non Matched Passwords).
```

### Scenarios For API Testing 
Scenario: Registration
```bash
1 - Creating Get request for "Register" Api.
  - Creating Interceptor (middle layer between client and server oin API testing). 
    my interception description:
    this request will be intercepted in the 5th weekday which is Thurday and this API will fail.
  - The response from the server will be save to txt file.
```
   
### How To Run the (API + UI together in one test suite) testing:
```bash
Run RegisterScenarios.xml as TestNG suite.
```

### Features:
```bash
1 - No need to include the driver.exe in the workspace. WebDriverManager is used for automating this simple task.
2 - Custom report generation with ScreenShots for failed test cases.
3 - No hard coded test data: TestDataGeneration class generate fresh random test data for the test cases.
3 - Data Driven: the test case sequence will run multiple times with different test data.
4 - As a result of using DDT model the assertion are assigned to a certain test sequence using Java Enums.
5 - Screenshots of failed test cases are taken and save to the workspace.
6 - standard and comprehensive exception and assertion handling (closing the browser, terminating excecution then generate report with these results).
```
### Components Used For This Testing Framework:
```bash
1 - The pages package:

contains (PageBase, AccountPage, LoginPage, RegisterationPage) classes:
- PageBase act as service provider for the web page.
- The rest of the Page classes contain WebElements, Operation on thw web element and also some test sequences.
- The rest of the Page classes inherit from PageBase to get the PageBase services.

2 - The tests package:

contains(TestBase, ValidRegisterationTest, InvalidRegisterationTest) classes:
- TestBase act as initiator (Set Up) and terminator (Tear Down) for the test cases execution.
- The rest of the test classes contain test cases with differnet priorities.
- the scenarios used are Invalid Registarion (Error Email Format + Non Matched Passwords).
- the scenarios used are Valid Registarion (Two Times With 2 Different Test Data).

3- The testDataGenerator package:

contains(TestDataGeneration) classe:
- To generate random data each function call.
- very useful in making the execution autonomous with out the developer to change the data each time.

5 - The utilities package:

contains(Helper) classe supports services in the framework.
- capturing ScreenShots.
- Buffering data to files.
- Customizing assertion and exception handling (closing the browser, terminating excecution then generate report with these results).

6 - The customReporter package:

contains(ExtentManager, ExtentTestManager, TestListener) classes.
- collaborate for custom report generation.
```

#### Reports
```bash
in the customReportFold folder
1 - customReportFold/customSuiteReport.html
```

### Limitations:
```bash
1 - The test executin is experienced only on google chrome.
2 - Not all the invalid registration scenarios are tested just two cases (Error Email Format + Non Matched Passwords).
```
