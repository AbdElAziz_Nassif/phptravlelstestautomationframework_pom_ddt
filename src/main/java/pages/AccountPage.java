package pages;
/*This class inherits the PageBase class 
 * this class contains the web elements of the account page and some methods to 
 * manipulate it and make use of it*/
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;



public class AccountPage extends PageBase{
	/*constructor of the class */
	public AccountPage(WebDriver driver) {
		/*from the PageBase Class*/
		super(driver);
	}
	
	/*web element contains user name as entered in the register form  */
	@FindBy(xpath = "//div[@class='col-md-8']//h3")
	private WebElement successFlagByUserName;
	/*web element contains header of the page to indicate that the user 
	 * profile pic is displayed in the account page  */
	@FindBy(xpath = "//div[@class='col-md-4']")
	private WebElement successFlagByUserProfPic;
	/*web element contains button to login or logout of the account  */
	@FindBy(xpath = "//a[@id='dropdownCurrency']//i[1]")
	private static WebElement accountBtnToLogInRout;
	/*web element contains button to logout of the account  */
	@FindBy(xpath = "//a[@href='https://www.phptravels.net/account/logout/']")
	private static WebElement logOutBtn;

	/*Method to check if user name is displayed or not*/
	public Boolean IsUserNameIsDisplayedOrNot()
	{
		wait.until(ExpectedConditions.visibilityOf(successFlagByUserName));
		return (successFlagByUserProfPic.isDisplayed());	
	}
	
	/*Method to check if user profile pic is displayed or not*/
	public Boolean IsProfilPicfRegisterdUserDisplayedOrNot()
	{
		wait.until(ExpectedConditions.visibilityOf(successFlagByUserProfPic));
		return (successFlagByUserProfPic.isDisplayed());	
	}
	/*Method to click MYACCOUNT button in the page*/
	private static void clickMyAccountButton()
	{
		clickButton(accountBtnToLogInRout);	
	}
	/*Method to click logout button in the page*/
	private static void clickLogOutButton()
	{
		clickButton(logOutBtn);	
	}
	/*Method to enable the user logging out of the account*/
	public void userLogoutFromAccount(WebDriver dvr) 
	{
		clickMyAccountButton();
		clickLogOutButton();	
	}
}
