package pages;
/*This class inherits the PageBase class 
 * this class contains the web elements of the login page and some methods to 
 * manipulate it and make use of it*/
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class LoginPage extends PageBase{

	/*constructor of the class */
	public LoginPage(WebDriver driver) {
		/*from the PageBase Class*/
		super(driver);
		// TODO Auto-generated constructor stub
	}


	/*web element contains email box to enter email*/
	@FindBy(xpath="//label[@class='pure-material-textfield-outlined float-none']//input[@type='email']")
	private static WebElement emailBox;
	/*web element contains password box to enter password*/
	@FindBy(xpath="//label[@class='pure-material-textfield-outlined float-none']//input[@type='password']")
	private static WebElement passwordBox;
	/*web element contains login button to confirm the login process */
	@FindBy(xpath="//button[@class='btn btn-primary btn-lg btn-block loginbtn']")
	private static WebElement loginBtn;
	/*web element contains login page header indication the login page is loaded */
	@FindBy(xpath="//div[@class='d-flex flex-column h-100']//h3[contains(text(),'Login')]")
	private static WebElement loginFlag;

	/*enter text to email box*/
	private static void fillLoginEmailBox(String email)
	{
		setTextElementText(emailBox, email);
	}
	/*enter text to password box*/
	private static void fillLoginpasswordBox( String pswrd)
	{
		setTextElementText(passwordBox, pswrd);
	}
	/*press login button to confirm login*/
	private static void clickLoginButton()
	{
		clickButton(loginBtn);
	}
	/*Method contains the steps of login process*/
	public void fillLoginForm(WebDriver dvr, String email, String password)
	{
		fillLoginEmailBox(email);
		fillLoginpasswordBox( password);
		clickLoginButton();
	}
	/*Method to check the login page is loaded successfully*/
	public Boolean checkIfLoginFlagIsDisplayed()
	{
		return loginFlag.isDisplayed();
	}

}
