package pages;
/*This Class Contains the Basic Selenium functions 
 * thos will act as service provider fot the rest of pages and the tests also
 * and the page will provide service fot the tests*/
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

/*This Class Contains the Basic Selenium functions 
 * thos will act as service provider fot the rest of pages and the tests also
 * and the page will provide service fot the tests*/
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PageBase {
	protected static WebDriverWait wait ;

	/*class to use Selenium methods like click/sendkeys ...etc 
	 * all pages will inherit from this class*/
	public PageBase(WebDriver driver)
	{
		/*init all webelements using initElements method from PageFactory class */
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 35);
	}
	/*function to wait on the visiblity of a webeleement*/
	public static void waitForElementToBeVisible(WebDriver local_driver, WebElement element)
	{
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/*clicking on webelements function(button/link)*/
	public static void clickButton(WebElement btn)
	{
		wait.until(ExpectedConditions.visibilityOf(btn));
		btn.click();
	}

	/*sending text to  webelements */
	public static void setTextElementText(WebElement textElem, String value)
	{
		wait.until(ExpectedConditions.visibilityOf(textElem));
		textElem.clear();
		textElem.sendKeys(value);
	}

	/*selecting on an option from drop deon list by visible text*/
	public static void selectCategory(WebElement catList, String visibleTxt)
	{
		Select select;
		wait.until(ExpectedConditions.visibilityOf(catList));
		select = new Select(catList);
		select.selectByVisibleText(visibleTxt);
	}
	/*function to upload file to the DOM Using Robot Class*/
	public void fileUpload (String path) {
		StringSelection strSelection = new StringSelection(path);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(strSelection, null);
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		robot.delay(150);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyPress(KeyEvent.VK_ENTER);
		//robot.delay(2000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.delay(200);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	/*scrolling to certain web element on the DOM*/
	public static void scrollToElement(WebDriver dvr,  WebElement webElm)
	{
		/*Casting WebDriver to JavascriptExecutor to execute javaScript code*/
		JavascriptExecutor js = (JavascriptExecutor) dvr;
		/*getting the location of the web element to scroll to it*/
		Point loc = webElm.getLocation();
		int x;
		int y;
		/*generating the x and y of the location*/
		x = loc.getX();
		y = loc.getY();
		/*scroll to the element*/
		js.executeScript("window.scrollBy(" + x + "," + y + ")" );
	}


}
