package pages;
/*This class inherits the PageBase class 
 * this class contains the web elements of the registration page and some methods to 
 * manipulate it and make use of it*/
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class RegisterationPage extends PageBase {
	/*constructor of the class */
	public RegisterationPage(WebDriver driver) {
		/*from the PageBase Class*/
		super(driver);
	}
	/*web element contains first name box to enter first name*/
	@FindBy(xpath = "//input[@name='firstname']")
	private static WebElement firstNameBox;
	/*web element contains last name box to enter last name*/
	@FindBy(xpath = "//input[@name='lastname']")
	private static WebElement lastNameBox;
	/*web element contains phone box to enter phone*/
	@FindBy(xpath = "//input[@name='phone']")
	private static WebElement mobileNumberBox;
	/*web element contains email box to enter email*/
	@FindBy(xpath = "//input[@name='email']")
	private static WebElement emailBox;
	/*web element contains password box to enter password*/
	@FindBy(xpath = "//input[@name='password']")
	private static WebElement passwordBox;
	/*web element contains confirm password box to enter confirm password*/
	@FindBy(xpath = "//input[@name='confirmpassword']")
	private static WebElement cnfrmPasswordBox;
	/*web element contains submit button to submit your form application*/
	@FindBy(xpath = "//button[@class='signupbtn btn_full btn btn-success btn-block btn-lg']")
	private static WebElement signUpSubmit;
	/*web element contains signUp page header to check page is loaded successfully*/
	@FindBy(xpath = "//div[@class='d-flex flex-column h-100']//h3[contains(text(),'Sign Up')]")
	private static WebElement SignUpFlag;
	/*web element contains warning message of non matching password entered by the user*/
	@FindBy(xpath = "//div[@class='alert alert-danger']//p[contains(text(),'Password not matching with confirm password.')]")
	public WebElement nonMatchingPasswordsMsg;
	/*web element contains warning message of invalid email format entered by the  user*/
	@FindBy(xpath = "//div[@class='alert alert-danger']//p[contains(text(),'The Email field must contain a valid email address.')]")
	public WebElement invalidEmailMsg;

	/*enter text to first name box*/
	private static void fillFirstNameField(String nameToBeEntered)
	{
		setTextElementText(firstNameBox, nameToBeEntered);
	}
	/*enter text to last name box*/
	private static void fillLastNameField(String nameToBeEntered)
	{
		setTextElementText(lastNameBox, nameToBeEntered);
	}
	/*enter text to phone box*/
	private static void fillPhoneField(String phne)
	{
		setTextElementText(mobileNumberBox, phne);
	}
	/*enter text to email box*/
	private static void fillEmailField(String mail)
	{
		setTextElementText(emailBox, mail);
	}
	/*enter text to password box*/
	private static void fillPasswordField(String pword)
	{
		setTextElementText(passwordBox, pword);
	}
	/*enter text to confirm password box*/
	private static void fillConfirmPasswordField(String cnfrmPword)
	{
		setTextElementText(cnfrmPasswordBox, cnfrmPword);
	}
	/*submit the registration form application*/
	private static void clickSubmitButton()
	{
		clickButton(signUpSubmit);
	}
	/*check if the SignUp header is displayed or not*/
	public Boolean isSignUpFlagDisplayed()
	{
		
		wait.until(ExpectedConditions.invisibilityOf(SignUpFlag));
		return SignUpFlag.isDisplayed();
	}
	/*check if the SignUp header is displayed or not*/
	public Boolean isNonMatchingPsswordErrorDisplayed()
	{
		wait.until(ExpectedConditions.invisibilityOf(nonMatchingPasswordsMsg));
		return nonMatchingPasswordsMsg.isDisplayed();
	}
	
	/*fill the form with data*/
	public void fillRegisterationForm(WebDriver dvr, String fname, String lname, String Phone, 
			String email, String pasword, String cnfrmPsword)
	{
		fillFirstNameField(fname);
		fillLastNameField(lname);
		fillPhoneField(Phone);
		fillEmailField(email);
		fillPasswordField(pasword);
		fillConfirmPasswordField(cnfrmPsword);
		/*scroll to WebElement*/
		scrollToElement(dvr, signUpSubmit);
		clickSubmitButton();
	}
}
