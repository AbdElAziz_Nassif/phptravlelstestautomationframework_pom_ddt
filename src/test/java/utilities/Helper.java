package utilities;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class Helper {
	/*method to capture screen shots and save it a destination
	 * TestBase Class will use it to take screen shots of the failed test cases*/
	public static void captureScreenShot(WebDriver dvr, String screenShotName)
	{
		/*setting the path of the taken screenshots*/
		Path dest = Paths.get("./screenshots/"+screenShotName+".png");

		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((TakesScreenshot)dvr).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (FileNotFoundException e) {
			handleAssertionOrException(dvr, "Helper", "captureScreenShotMethod"
					, "Exception is caught while screenshot" + e.getMessage());
		} catch (IOException e) {
			handleAssertionOrException(dvr, "Helper", "captureScreenShotMethod"
					, "Exception is caught while screenshot" + e.getMessage());
		}
	}
	/*method to append generate files as the user needs*/
	public static void usingBufferedWritter(WebDriver dvr, String textToAppend, String folderName
			, String fileName, String ClassName, String testMthod, String log) 
	{  
		Path dest = Paths.get("./" + folderName +"/"+fileName);
		try {
			/*creating directory*/
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			/*buffer data to a file*/
			BufferedWriter writer = new BufferedWriter(
					new FileWriter(dest.toString(), true)  //Set true for append mode
					);  
			writer.write(textToAppend);
			writer.close();
		}
		catch (IOException e) {
			handleAssertionOrException(dvr, "Helper", "usingBufferedWritterMethod", "IOException happened");
		}
	}
	/*method to obligate failure if exception happened or assertion failed*/
	public static void handleAssertionOrException(WebDriver dvr, String ClassName, String methodname, String log)
	{
		dvr.quit();
		System.out.println("Failure in Assertion" + ClassName + "->" + methodname + "->" + log);
		Assert.fail("failure Assertion or exception" + "->" + ClassName + "->" + methodname + "->" + log);
		//System.exit(-1);
	}
	/* Method for the recovery from assertion and getting the Class then method then log by programmer
	 * if assertion condition failed stop, close the browser and obligate failure and
	 * generate the report accordingly
	 */
	public static void AssertionWithRecovery(WebDriver dvr, Boolean conditionToAssert, String ClassName,
			String methodname, String log)
	{
		if(conditionToAssert == true)
		{
			Assert.assertTrue(true);
		}
		else {
			handleAssertionOrException(dvr, ClassName, methodname, log);
		}
	}
}
