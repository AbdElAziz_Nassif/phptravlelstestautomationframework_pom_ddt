package apiTest;

/*This class make use of httpClient java class which is added using maven to test API's
 * and getting the http response of the API's*/
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.Test;

import customReporter.ExtentTestManager;
import tests.TestBase;
import utilities.Helper;
/*Api Testing Httpclient*/
public class RegisterRequestInterceptor extends TestBase {
	HttpResponse httpResponse;
	String httpResponseAsString;
	@Test(priority = 1)
	public void generateHttpResponseForRegisterApiRequst(Method method) 
			throws ClientProtocolException, IOException
	{
		ExtentTestManager.startTest(method.getName(), "generate api response of register api request");
		try {
			/*overriding the process method from the HttpRequestInterceptor interface*/
			HttpRequestInterceptor requestInterceptor = new HttpRequestInterceptor() {
				public void process(HttpRequest request, HttpContext context) throws
				HttpException, IOException {
					/*if this any day except Thursday 5th week day ----> the server will handle the request*/
					/*if this day is 5th day ----> the request will be hindered by the interceptor*/
					Calendar cal = Calendar.getInstance();
					int weekDay = cal.get(cal.DAY_OF_WEEK);
					if (weekDay == 5)
					{
						Helper.handleAssertionOrException(driver, "RegisterRequestInterceptor",
								"@Test->generateHttpResponseForRegisterApiRequst", "stop request --> it's friday 5th week day");
					}
				}
			};
			
			/*Creating a client as CloseableHttpClient class object*/
			CloseableHttpClient httpclient =
					HttpClients.custom().addInterceptorFirst(requestInterceptor).build();
			
			/*Creating a request*/
			HttpGet httpGetRequest = new HttpGet(api_url);
			/*execute the request*/
			httpResponse = httpclient.execute(httpGetRequest);
			/*Assert that the httpResponse  response is not empty*/
			Helper.AssertionWithRecovery(driver, (httpResponse.toString() != null),"RegisterRequestInterceptor",
					"@Test->generateHttpResponseForRegisterApiRequst", "Assert on http respnse is empty failed");
			/*Assert that the http response status is success (200)*/
			Helper.AssertionWithRecovery(driver, (httpResponse.getStatusLine().getStatusCode() == 200),
					"RegisterRequestInterceptor", "@Test->generateHttpResponseForRegisterApiRequst", 
					"Assert on http respnse is 200 as successful response from the server");
			/*saving the http response as String*/
			httpResponseAsString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			/*logging the http response*/
			
			/*saving the header response in HeadersResponse.txt file */
			Helper.usingBufferedWritter(driver, httpResponseAsString, "httpResponseFold" ,
					"HttpResponse.html", 
					"RegisterRequestInterceptor", "@Test->generateHttpResponseForRegisterApiRequst",
					"failed to generate http response of register api requst");
		}
		catch(Exception e){
			/*If any type of exception happens, an assertion will fail and stops the tests*/
			Helper.handleAssertionOrException(driver
					, "RegisterRequestInterceptor" 
					,"@Test->generateHttpResponseForRegisterApiRequst"
					,e.getMessage() + " " + e.getStackTrace());
		}
	}
	


}
