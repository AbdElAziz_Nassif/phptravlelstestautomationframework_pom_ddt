package tests;
/*This class uses selenium and testng to organize the tests orders, create the driver object and
 * capturing screenshots in case of test failure*/
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;
import utilities.Helper;

public class TestBase {
	/*The driver of the Test cases */
	public static WebDriver driver;
	/*explicit wait if any test cases need it */
	public static WebDriverWait wait;
	/*folder to save the downloads if any */
	public static String downloadPath = System.getProperty("user.dir")+"\\downloads";
	/*the website under test */
	protected static String aut_url = "https://www.phptravels.net/register";
	/*the api under test */
	protected static String api_url = "https://www.phptravels.net/register";


	/*method to return the WebDriver Element */
	public WebDriver getDriver() {
		return driver;
	}
	/*method to set up the fire fox browser options*/
	public static FirefoxOptions firefoxOpiton()
	{
		FirefoxOptions myBrowserOptions = new FirefoxOptions();
		myBrowserOptions.addPreference("browser.download.folderList", 2);
		myBrowserOptions.addPreference("browser.download.dir", downloadPath);
		myBrowserOptions.addPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
		myBrowserOptions.addPreference("browser.download.manager.showWhenStarting", false);
		myBrowserOptions.addPreference("pdfjs.disabled", true);
		return myBrowserOptions;
	}

	/*method to set up the chrome browser options*/
	public static ChromeOptions chromeOption() {
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default.content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadPath);
		options.setExperimentalOption("prefs", chromePrefs);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return options;
	}

	/*will run before the suite only once to configure the browser to be used 
	 * --> considering the default browser firefox if no testng.xml is provided*/
	@BeforeSuite
	@Parameters({"browser"})
	public void startDriver(@Optional("firefox")String browserName)
	{
		if(browserName.equalsIgnoreCase("chrome"))
		{
			/*set up the firefox browser to be the browser used in tests*/
			WebDriverManager.chromedriver().setup();

			ChromeOptions options = new ChromeOptions();
			options.setHeadless(true);
			//Create driver object for Chrome
			driver = new ChromeDriver(options);
		}
		else if(browserName.equalsIgnoreCase("firefox"))
		{
			/*set up the chrome browser to be the browser used in tests*/
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver(firefoxOpiton());
		}
		wait = new WebDriverWait(driver, 30);
		/*maximize the window screen*/
		driver.manage().window().maximize();
		/*give the driver 15 seconds before announcing failure of finding any web element*/
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}

	/*will run after the suite only once to quit all the browser windows and end the tests*/
	@AfterSuite(enabled = true)
	public void stopDriver()
	{
		driver.quit();
	}

	/*screenShotFailure will run after every test case to  check if it failed or not
	 * and if failed it will capture it */
	@AfterMethod
	public void screenShotFailure(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE )
		{
			/* take screen shot if the status is failed */
			System.out.println("Capturing Screenshot In Progress .........");
			Helper.captureScreenShot(driver, result.getName());
		}
	}

}
