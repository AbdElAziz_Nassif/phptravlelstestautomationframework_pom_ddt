package tests;
import java.lang.reflect.Method;
/*This class inherits the TestBase Class and contains the valid scenario for the 
 * user registeration form with no hard-coded data*/
import java.util.NoSuchElementException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import customReporter.ExtentTestManager;
import pages.AccountPage;
import pages.LoginPage;
import pages.RegisterationPage;
import testDataGenerator.TestDataGeneration;
import utilities.Helper;


public class ValidRegisterationTest extends TestBase{
	/*Declare objects of useful classes for the tests cases of  this class
	 * those declared objects will be defined in the @BeforeClass Method Below*/
	RegisterationPage regPageObj;
	AccountPage acountPageObj;
	LoginPage loginObj;
	
	/*Two Same Valid Registration Scenarios */
	@DataProvider(name = "ValidRegisterInfo")
	public static Object[][] credentials() {
		
		TestDataGeneration td = new TestDataGeneration();

		//Columns - Number of parameters in test data.
		Object[][] data = new Object[2][6];

		data[0][0] = td.generateRandomName(5);
		data[0][1] = td.generateRandomName(5);
		data[0][2] = td.generateRandomPhoneNumber();
		data[0][3] = td.generateRandomEmail(6);
		data[0][4] = td.generateRandomPassword();
		data[0][5] = data[0][4] ;
		
		data[1][0] = td.generateRandomName(5);
		data[1][1] = td.generateRandomName(5);
		data[1][2] = td.generateRandomPhoneNumber();
		data[1][3] = td.generateRandomEmail(6);
		data[1][4] = td.generateRandomPassword();
		data[1][5] = data[1][4] ;
		
		return data;
	}
	
	/*This @BeforeClass method will run once after @BeforeSuite and Before the class which contains it*/
	@BeforeClass
	public void navigateToSite()
	{
		try {
			/*create objects of useful classes for the tests cases of  this class*/
			regPageObj = new RegisterationPage(driver);
			acountPageObj = new AccountPage(driver);
			loginObj = new LoginPage(driver);
		}
		catch (Exception e) {
			/*handle any other Exception and getting the log of it */
			Helper.handleAssertionOrException(driver
					, "RegisterationTest", "@BeforeClass->navigateToSite", e.getMessage() + " " + e.getStackTrace());
		}
	}
	
	/*userRegister Test Case is used to fill the registration form with all valid data*/
	@Test(priority = 3, dataProvider = "ValidRegisterInfo")
	public void userRegister(String fname, String lname, String Phone, String email, String pasword, String cnfrmPsword
								,Method method)
	{
		try {
			ExtentTestManager.startTest(method.getName(), "user register entering valid data");
			
			/*navigate to the page https://www.phptravels.net/register*/
			driver.get(aut_url);
			
			/*fill the registration from function in RegisterationPage class*/
			regPageObj.fillRegisterationForm(driver, fname, lname, Phone, email, pasword, cnfrmPsword);
			
			
			/*assert that the user name of the registered account is displayed*/
			Helper.AssertionWithRecovery(driver, (acountPageObj.IsUserNameIsDisplayedOrNot() == true), 
					"RegisterationTest Class", "@Test->userRegister", "Assert On User Name");
			
			/*assert that the url of the registered account opens*/
			Helper.AssertionWithRecovery(driver, driver.getCurrentUrl().contains("https://www.phptravels.net/account/"), 
					"RegisterationTest Class", "@Test->userRegister", "Assert on current url failed");
			/*assert that the profile picture of the user is displayed*/
			Helper.AssertionWithRecovery(driver, acountPageObj.IsProfilPicfRegisterdUserDisplayedOrNot(),
					"RegisterationTest Class", "@Test->userRegister", "Assert on profile picture appears failed");
			
			/*Log out from the registerd account*/
			acountPageObj.userLogoutFromAccount(driver);
			/*assert that the login header is displayed so that indicating that the 
			 * user can login */
			Helper.AssertionWithRecovery(driver, loginObj.checkIfLoginFlagIsDisplayed(),"RegisterationTest Class",
					"@test->userLogout", "Assert on Login header flag failed");
			
			/*Log in again to the registerd account*/
			loginObj.fillLoginForm(driver, email, pasword);
			
			/*assert that the user name of the registered account is displayed*/
			Helper.AssertionWithRecovery(driver, (acountPageObj.IsUserNameIsDisplayedOrNot() == true), 
					"RegisterationTest Class", "@Test->userRegister", "Assert on User name");
			
			/*assert that the url of the registered account opens*/
			Helper.AssertionWithRecovery(driver, driver.getCurrentUrl().contains("https://www.phptravels.net/account/"),
					"RegisterationTest Class",	"@test->userLogIn", "Assert on current url failed");
			/*assert that the profile picture of the user is displayed*/
			Helper.AssertionWithRecovery(driver, acountPageObj.IsProfilPicfRegisterdUserDisplayedOrNot(),
					"RegisterationTest Class", "@test->userLogIn", "Assert on the profile pic displayed failed");
			/* log out again for the sake of Registering again. */
			acountPageObj.userLogoutFromAccount(driver);

			Helper.AssertionWithRecovery(driver, loginObj.checkIfLoginFlagIsDisplayed(),"RegisterationTest Class",
					"@test->userLogout", "Assert on Login header flag failed");
		}
		catch (NoSuchElementException e) {
			/*handle NoSuchElementException is famous Exception in the web testing automation */
			Helper.handleAssertionOrException(driver
					, "RegisterationTest", "@Test->userRegister", e.getMessage() + " " + e.getStackTrace());
		}
		catch (Exception e) {
			/*handle any other Exception and getting the log of it */
			Helper.handleAssertionOrException(driver
					, "RegisterationTest", "@Test->userRegister", e.getMessage() + " " + e.getStackTrace());
		}
	}
	
	@AfterClass(enabled = true)
	public void endThisClassTests()
	{

	}
	
	

}
