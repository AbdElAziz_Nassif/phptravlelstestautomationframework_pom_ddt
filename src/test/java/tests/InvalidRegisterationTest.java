package tests;
import java.lang.reflect.Method;

/*This class inherits the TestBase Class and contains the Invalid scenario for the 
 * user registeration form with no hard-coded data*/
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import customReporter.ExtentTestManager;
import pages.AccountPage;
import pages.LoginPage;
import pages.PageBase;
import pages.RegisterationPage;
import testDataGenerator.TestDataGeneration;
import utilities.Helper;

public class InvalidRegisterationTest extends TestBase{

	/*Declare objects of useful classes for the tests cases of  this class
	 * those declared objects will be defined in the @BeforeClass Method Below*/
	RegisterationPage regPageObj;
	AccountPage acountPageObj;
	LoginPage loginObj;
	
	/*  Enum To determine which asssertions to be used at the end of the test case sequence
    Every Time the Tests of sequence is the same, 
    but the test data are not which will affect the assertion results 
	 */
	enum testCasesStates {EMAIL_FORMAT_ERROR, NON_MATCH_PASSWORDS }
	/*varaible to help the Enum Doing its goal */
	testCasesStates states = testCasesStates.EMAIL_FORMAT_ERROR;

	/*userRegisterWithNonMathchingPasswordsOrInvalidEmailFormat 
	 * Test Case is used to fill the registration form with all Invalid data
	 * Two Scenarios 
	 * 1 - user enter invalid email format in email box
	 * 2 - user enter two non matched passwords in the form
	 * */
	@DataProvider(name = "InvalidRegisterInfo")
	public static Object[][] credentials() {
		TestDataGeneration td = new TestDataGeneration();

		//Columns - Number of parameters in test data.
		Object[][] data = new Object[2][6];

		/*Generate invalid Registration data (Wrong Email Formatting Only) */
		data[0][0] = td.generateRandomName(5);
		data[0][1] = td.generateRandomName(5);
		data[0][2] = td.generateRandomPhoneNumber();
		data[0][3] = td.generateRandomName(5);
		data[0][4] = td.generateRandomPassword();
		data[0][5] = data[0][4] ;
		/*Generate invalid Registration data (different Password Only) */
		data[1][0] = td.generateRandomName(5);
		data[1][1] = td.generateRandomName(5);
		data[1][2] = td.generateRandomPhoneNumber();
		data[1][3] = td.generateRandomEmail(6);
		data[1][4] = td.generateRandomPassword();
		data[1][5] = td.generateRandomPassword();

		return data;
	}
	/*This @BeforeClass method will run once after @BeforeSuite and Before the class which contains it*/
	@BeforeClass
	public void navigateToSite()
	{
		try {
			/*navigate to the page https://www.phptravels.net/register*/
			/*create objects of useful classes for the tests cases of  this class*/
			regPageObj = new RegisterationPage(driver);
			acountPageObj = new AccountPage(driver);
			loginObj = new LoginPage(driver);
		}
		catch (Exception e) {
			/*handle any other Exception and getting the log of it */
			Helper.handleAssertionOrException(driver
					, "InvalidRegisterationTest", "@BeforeTest->navigateToSite"
					, e.getMessage() + " " + e.getStackTrace());
		}
	}



	/*userRegister Test Case is used to fill the registration form with all valid data*/
	@Test(priority = 2, dataProvider = "InvalidRegisterInfo")
	public void userRegister(String fname, String lname, String Phone, String email, String pasword, String cnfrmPsword
			,Method method)
	{
		try {
			ExtentTestManager.startTest(method.getName(), "user register entering invalid data");

			/*navigate to the page https://www.phptravels.net/register*/
			driver.get(aut_url);

			/*fill the registration from function in RegisterationPage class*/
			regPageObj.fillRegisterationForm(driver, fname, lname, Phone, email, pasword, cnfrmPsword);
			
			/*Check For The Wrong the error message content displayed from the site for the 
				 * non matched password entered by the user*/
			if(states == testCasesStates.EMAIL_FORMAT_ERROR)
			{
				states = testCasesStates.NON_MATCH_PASSWORDS;
				wait.until(ExpectedConditions.visibilityOf(regPageObj.invalidEmailMsg));				
				Helper.AssertionWithRecovery(driver, 
						(regPageObj.invalidEmailMsg.getText().contains("The Email field must contain a valid email address.")),
						"InvalidRegisterationTest Class", "userRegisterWithInvalidEmail", 
						"the message of invalid email format is not displayed");
			}
			else if(states == testCasesStates.NON_MATCH_PASSWORDS) {
				wait.until(ExpectedConditions.visibilityOf(regPageObj.nonMatchingPasswordsMsg));

				/*Assert with the warning message content displayed from the site for the 
				 * non matched password entered by the user*/
				Helper.AssertionWithRecovery(driver, 
						(regPageObj.nonMatchingPasswordsMsg.getText().contains("Password not matching with confirm password.")),
						"InvalidRegisterationTest Class", "userRegisterWithNonMathchingPasswordsTestMthod", 
						"the message of two non matching passwors is not displayed");
			}
		}
		catch (NoSuchElementException e) {
			/*handle NoSuchElementException is famous Exception in the web testing automation */
			Helper.handleAssertionOrException(driver
					, "RegisterationTest_NoElem", "@Test->userRegister", e.getMessage() + " " + e.getStackTrace());
		}
		catch (Exception e) {
			/*handle any other Exception and getting the log of it */
			Helper.handleAssertionOrException(driver
					, "RegisterationTest", "@Test->userRegister", e.getMessage() + " " + e.getStackTrace());
		}
	}

	@AfterClass
	public void endThisClassTests()
	{

	}



}

