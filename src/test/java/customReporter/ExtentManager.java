package customReporter;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {

  private static ExtentReports extentReport;

  public synchronized static ExtentReports getReporter() {
      if (extentReport == null) {
          /*creating HTML reporting file location*/
          String workingDir = System.getProperty("user.dir");
          if (System.getProperty("os.name").toLowerCase().contains("win")) {
        	  extentReport = new ExtentReports(workingDir + "\\customReportFold\\customSuiteReport.html", true);
          }
      }
      return extentReport;
  }
}
