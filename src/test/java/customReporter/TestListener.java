package customReporter;

import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import tests.TestBase;

 
public class TestListener extends TestBase implements ITestListener {
 
	/*Getting the method name for logging and debugging*/
    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
    /*method called before the suite only once*/
    public void onStart(ITestContext iTestContext) {
        System.out.println("Logging onStart of suite <---------> " + iTestContext.getName());
        iTestContext.setAttribute("WebDriver", this.driver);
    }
 
    /*method called after the suite only once*/
    public void onFinish(ITestContext iTestContext) {
        System.out.println("Logging onFinish of suite <--------->" + iTestContext.getName());
        //Do tier down operations for extentreports reporting!
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
    /*method called before every test method in the suite*/
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("Logging onTestStart method " + getTestMethodName(iTestResult) + " start");
    }
 
    /*method called after only passed test methods in the suite*/
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("Logging onTestSuccess method " + getTestMethodName(iTestResult) + " succeed");
        ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
    }
 
    /*method called after only failed test methods in the suite*/
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("Logging onTestFailure method " + getTestMethodName(iTestResult) + " failed");
 
        /*Get driver from TestBase and assign to local webDriver variable*/
        Object testClass = iTestResult.getInstance();
        WebDriver webDriver = ((TestBase) testClass).getDriver();
 
        /*Take base64Screenshot screenshot to the right of the report in case of failed tests*/
        String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) webDriver).
            getScreenshotAs(OutputType.BASE64);
 
        /*ExtentReports log and screenshot operations for failed tests.*/
        ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed",
            ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
    }
 
    /*method called after only skipped test methods in the suite*/
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println("Logging onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");
        /*ExtentReports log operation for skipped tests.*/
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
    }
    /*method called after only skipped test methods in the suite*/
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		/* not used but cant be removed because of inheriting the interface*/
	}
 
}
