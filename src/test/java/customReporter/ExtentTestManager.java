package customReporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import java.util.HashMap;
import java.util.Map;
 

public class ExtentTestManager {
	/*map to hold information of method id and ExtentTest instances*/
    static Map extentReportTestMap = new HashMap();
    /*instance of ExtentReports created by calling getReporter() method from ExtentManager*/
    static ExtentReports extentReport = ExtentManager.getReporter();
 
    
    
    
    /*starting the method id to reserve a place for it in the report
     * return ExtentTest instance in extentTestMap by using current thread id*/
    public static synchronized ExtentTest getTest() {
        return (ExtentTest) extentReportTestMap.get((int) (long) (Thread.currentThread().getId()));
    }
    /*method for tier down operation for tests
     * test ends and ExtentTest instance got from extentTestMap via current thread id*/
    public static synchronized void endTest() {
    	extentReport.endTest((ExtentTest) extentReportTestMap.get((int) (long) (Thread.currentThread().getId())));
    }
    /*starting the test and reserve a place for it in the map for the report
     * an instance of ExtentTest created and put into extentTestMap with current thread id*/
    public static synchronized ExtentTest startTest(String testName, String desc) {
        ExtentTest test = extentReport.startTest(testName, desc);
        extentReportTestMap.put((int) (long) (Thread.currentThread().getId()), test);
        return test;
    }
}
